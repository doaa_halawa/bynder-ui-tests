/// <reference types="cypress" />
class dashboardPage {
  profileDropMenu = '[class="admin-dropdown profile"]';

  logout() {
    cy.get(this.profileDropMenu).click();
    cy.findByText("Logout")
      .should("be.exist")
      .click();
    return this;
  }
}

export default dashboardPage;
