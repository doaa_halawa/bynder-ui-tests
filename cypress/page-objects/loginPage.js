/// <reference types="cypress" />
class loginPage {
  usernameText = '[id="inputEmail"]';
  passwordText = '[id="inputPassword"]';
  loginButton = '[class^="login-btn"]';
  notificationBanner = '[class="notification"]';
  loginContainer = '[class^="login-container"]';
  languageList = 'ul[class="single"] li';
  languageButton = '[class^="admin-options"]';
  lostPasswordLink = '[class="lost-password"] a';

  login(username, password) {
    cy.get(this.usernameText)
      .clear()
      .type(username);
    cy.get(this.passwordText)
      .clear()
      .type(password);
    cy.get(this.loginButton).click();
    return this;
  }
}

export default loginPage;
