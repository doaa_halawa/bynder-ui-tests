export const generateRandomNumber = (len) => {
  let x = "";
  for (let i = 0; i < len; i++) {
    x += Math.floor(Math.random() * 9) + 1;
  }
  return x;
};

export const generateEmail = () => {
  return `automation+${generateRandomNumber(7)}@gmail.com`;
};
