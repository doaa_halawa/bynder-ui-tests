/// <reference types="cypress" />
import LoginPage from "../../page-objects/loginPage.js";
import DashboardPage from "../../page-objects/dashboardPage.js";
import VerifyPage from "../../page-objects/verifyPage.js";
import languagesList from "../../fixtures/loginPageLanguages.json";
import { generateEmail } from "../../support/data";

describe("login", () => {
  let loginPage = new LoginPage();
  let dashboardPage = new DashboardPage();
  let verifyPage = new VerifyPage();
  let username = Cypress.config("validUsername");
  let password = Cypress.config("validPassword");

  before(() => {
    cy.bynderTrialVisit();
  });

  it("Verify Login scenario", function() {
    //This is not a proper way to get username/password
    //we should create new account from scratch using API/UI
    //instead of depending on stored DB value
    loginPage.login(username, password);
    cy.url().should("include", "account/dashboard/");
    cy.get(dashboardPage.profileDropMenu).should("be.exist");
  });

  it("Verify Logout scenario", function() {
    dashboardPage.logout();
    cy.get(dashboardPage.profileDropMenu).should("not.exist");
    cy.get(loginPage.loginContainer).should("be.exist");
  });

  it("Verify invalid login scenario", function() {
    cy.reload();
    loginPage.login(generateEmail(), `invalid${password}`);
    cy.get(loginPage.notificationBanner)
      .should("be.exist")
      .contains("You have entered an incorrect username or password.");
  });

  it("Verify invalid login several times using same username", function() {
    let invalidUsername = generateEmail();
    for (let i = 0; i < Cypress.config("numberOfInvalidLogin"); i++) {
      loginPage.login(invalidUsername, `invalid${password}`);
    }
    cy.url().should("include", "/verify");
    cy.get(verifyPage.header)
      .should("be.exist")
      .should("contain.text", "Security Check");
    cy.get(verifyPage.capcha).should("be.exist");
  });

  languagesList.forEach((language) => {
    it(`Verify content for ${language.name} login page`, function() {
      cy.bynderTrialVisit()
        .get(loginPage.languageButton)
        .click();
      cy.get(loginPage.languageList)
        .eq(language.index)
        .click();
      cy.get(loginPage.usernameText)
        .invoke("attr", "placeholder")
        .should("contain", `${language.username}`);
      cy.get(loginPage.passwordText)
        .invoke("attr", "placeholder")
        .should("contain", `${language.password}`);
      cy.get(loginPage.lostPasswordLink).should(
        "contain.text",
        `${language.lostPassword}`
      );
      cy.get(loginPage.loginButton).should(
        "contain.text",
        `${language.loginButton}`
      );
    });
  });
});
