FROM cypress/included:6.8.0

# Renders <filename>.json.tmpl files using env vars
RUN npm install -g envsub

COPY package.json package.json
RUN npm i

COPY ./cypress ./cypress
COPY cypress.json ./cypress.json
