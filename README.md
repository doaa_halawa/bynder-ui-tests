# Bynder Ui tests

Bynder ui tests written using Cypress end to end testing framework

# Install and run tests

Install all the packages `npm install`

- To open Cypress dashboard:

`npx cypress open [environment] [options]`

- Default run environment is set to `wave-trial`

| Environment | Command                                                |
| ----------- | ------------------------------------------------------ |
| wave-trial  | Set by default https://wave-trial.getbynder.com/login/ |
| Local       | `--env baseURL=https://xxxxxxx.getbynder.com/login/`   |
| Development | `--env baseURL=https://xxxxxxx.getbynder.com/login/`   |
| Production  | `--env baseURL=https://xxxxxxx.getbynder.com/login/`   |

### Options:

| Option                | Command                                       |
| --------------------- | --------------------------------------------- |
| Browser               | `--browser <browser_name>`                    |
| Specific set of tests | `--config integrationFolder=<path_to_folder>` |

### Examples:

Open Cypress dashboard on wave-trial, chrome browser, running specific folder:

- `npx cypress open --browser chrome --config integrationFolder=cypress/integration/examples`

# Rerun and Repeat

- To Run a Single Specification For Multiple times:

  `npx cypress-repeat run -n <N> ... rest of "cypress run" arguments`

  Eg: `npx cypress-repeat run -n "3" --spec "<file_Path>"`

  Below, will run Cypress up to <N> times until the first successful exit.

  `npx cypress-repeat run -n <N> --until-passes ... rest of "cypress run" arguments`

  Eg: `npx cypress-repeat run -n "3" --until-passes --spec "<file_Path>"`

# Using Docker

If you want to use docker then build the docker image

`docker build --tag bynder-ui-tests:latest .`

`docker run --ipc=host -t bynder-ui-tests .`
