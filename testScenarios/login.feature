Feature: Login/Logout scenarios
  Description: As a user I want view login page So that I can perform tasks related to it

  Background:
    Given Create an account in wave-trial site

  Scenario Outline: Login with valid creditials to wave-trial site
    Given Go to wave-trial login page
    When I insert user credintials "<Username>" and "<Password>" and click login
    Then I should be able to login successfully

    Examples:
      | Scenario        | Username      | Password      |
      | validCreditials | validUsername | validPassword |


  Scenario: Logout from wave-trial site
    Given As a logged in user click on your username in the header
    When I click on logout button
    Then I should be logged out and redirected to login page


  Scenario Outline: Login with invalid creditials to wave-trial site
    When I insert user invalid credintials "<Username>" and "<Password>" and click login
    Then Error message should be displayed

    Examples:
      | Scenario          | Username        | Password        |
      | invalidCreditials | validUsername   | invalidPassword |
      | invalidCreditials | invalidUsername | validPassword   |


  Scenario Outline: Change login page language
    When Click on language button and change language to "<Scenario>"
    Then All page text should be changed "<Username>", "<Password>" , "<loginButton>" , "<lostPassword>"

    Examples:
      | Scenario | Username                     | Password       | loginButton | lostPassword                               |
      | English  | "Email/Username"             | "Password"     | "Login"     | "Lost password?"                           |
      | Dutch    | "E-mail/Gebruikersnaam"      | "Wachtwoord"   | "Inloggen"  | "Wachtwoord vergeten?"                     |
      | French   | "E-mail/Identifiant"         | "Mot de passe" | "Connexion" | "Vous avez perdu votre mot de passe ?"     |
      | German   | "E-Mail/Benutzername"        | "Passwort"     | "Login"     | "Passwort vergessen?"                      |
      | Italian  | "Email/Username"             | "Password"     | "Login"     | "Password dimenticata?"                    |
      | Spanish  | "Correo electrónico/usuario" | "Contraseña"   | "Conexión"  | lostPas"¿Has olvidado la contraseña?"sword |

